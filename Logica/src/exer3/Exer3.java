/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exer3;

import java.awt.Event;
import static java.lang.System.in;
import java.util.Scanner;

/**
 *
 * @author henri
 */
public class Exer3 {

    public static void main(String[] args) {

        int numInt;
        String tamStr;
        String sum = "";
        String numRom;
        String saidaStr = "";

        System.out.println("Digite o número desejado: ");
        Scanner sc = new Scanner(in);
        numInt = sc.nextInt();
        tamStr = numInt + "";

        // System.out.println("Entrada: " + numInt);
        int vetorN[] = new int[tamStr.length()];

        //  System.out.println("For: ");
        for (int i = 0; i < tamStr.length(); i++) {
            // System.out.println(i);

            vetorN[tamStr.length() - (i + 1)] = ' ' + tamStr.charAt(i) - 80;

            //System.out.println(tamStr.length()-i+" valor:" +   vetorN[tamStr.length()-(i+1)] );
            if (tamStr.length() - i == 4) {
                if (vetorN[tamStr.length() - (i + 1)] == 1) {

                    saidaStr += "M";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 2) {

                    saidaStr += "MM";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 3) {

                    saidaStr += "MMM";
                }

                if (vetorN[tamStr.length() - (i + 1)] >= 4) {

                    System.out.println("Digite um numero menor que 4000");
                    break;
                }

            }

            if (tamStr.length() - i == 3) {
                if (vetorN[tamStr.length() - (i + 1)] == 1) {

                    saidaStr += "C";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 2) {

                    saidaStr += "CC";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 3) {

                    saidaStr += "CCC";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 4) {

                    saidaStr += "CD";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 5) {

                    saidaStr += "D";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 6) {

                    saidaStr += "DC";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 7) {

                    saidaStr += "DCC";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 8) {

                    saidaStr += "DCCC";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 9) {

                    saidaStr += "CM";
                }

            }

            if (tamStr.length() - i == 2) {
                if (vetorN[tamStr.length() - (i + 1)] == 1) {

                    saidaStr += "X";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 2) {

                    saidaStr += "XX";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 3) {

                    saidaStr += "XXX";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 4) {

                    saidaStr += "XL";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 5) {

                    saidaStr += "L";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 6) {

                    saidaStr += "LX";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 7) {

                    saidaStr += "LXX";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 8) {

                    saidaStr += "LXXX";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 9) {

                    saidaStr += "XC";
                }

            }

            if (tamStr.length() - i == 1) {

                if (vetorN[tamStr.length() - (i + 1)] == 1) {

                    saidaStr += "I";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 2) {

                    saidaStr += "II";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 3) {

                    saidaStr += "III";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 4) {

                    saidaStr += "IV";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 5) {

                    saidaStr += "V";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 6) {

                    saidaStr += "VI";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 7) {

                    saidaStr += "VII";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 8) {

                    saidaStr += "VIII";
                }

                if (vetorN[tamStr.length() - (i + 1)] == 9) {

                    saidaStr += "IX";
                }

            }

        }

        if (!saidaStr.equals("")) {
            System.out.println("Saída= " + saidaStr);
        }
    }

    // System.out.println("SaidaString: " + op);
}
